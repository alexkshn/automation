import com.thoughtworks.xstream.XStream;
import org.apache.poi.hssf.usermodel.*;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.apache.poi.ss.usermodel.*;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;

public class Main {


    public static void main(String[] args) throws Exception {
        URLConnection hp = new URL("http://www.bnm.md/en/official_exchange_rates?get_xml=1&date=27.09.2017").openConnection();
        InputStream stream = hp.getInputStream();

        BufferedReader rd = new BufferedReader(new InputStreamReader(stream));
        StringBuilder resp = new StringBuilder(); // or StringBuffer if Java version 5+
        String line;
        while ((line = rd.readLine()) != null) {
            resp.append(line);
            resp.append('\n');
        }
        rd.close();

        XStream xstream = new XStream(new DomDriver());
        xstream.useAttributeFor(ValCurs.class, "Date");
        xstream.useAttributeFor(ValCurs.class, "name");
        xstream.addImplicitCollection(ValCurs.class, "list");
        ValCurs valCurs = new ValCurs();
        valCurs = (ValCurs) xstream.fromXML(resp.toString());
//        URL hp = new URL("http://bnm.md/en/official_exchange_rates?get_xml=1&date=22.03.2019");
//        HttpURLConnection hpCon = (HttpURLConnection) hp.openConnection();
//        URLConnection connection = new URL("http://bnm.md/en/official_exchange_rates?get_xml=1&date=22.03.2019").openConnection();
////        CloseableHttpClient client = HttpClients.createDefault();
////        HttpGet request = new HttpGet("http://bnm.md/en/official_exchange_rates?get_xml=1&date=22.03.2019");
////        CloseableHttpResponse response = client.execute(request);
//        InputStream stream = connection.getInputStream();
//
//        BufferedReader rd = new BufferedReader(new InputStreamReader(stream));
//        StringBuilder resp = new StringBuilder(); // or StringBuffer if Java version 5+
//        String line;
//        while ((line = rd.readLine()) != null) {
//            resp.append(line);
//            resp.append('\n');
//        }
//        rd.close();
//        System.out.println(resp);
//        XStream xstream = new XStream(new DomDriver());
//       // xstream.useAttributeFor(Valute.class, "ID");
//        xstream.useAttributeFor(ValCurs.class, "Date");
//        xstream.useAttributeFor(ValCurs.class, "name");
//        xstream.addImplicitCollection(ValCurs.class, "list");
//
//        ValCurs valCurs = (ValCurs) xstream.fromXML(resp.toString());
//
//        Collections.sort(valCurs.getList(), new Comparator<Valute>() {
//            public int compare(Valute v1, Valute v2) {
//                return v1.getName().compareTo(v2.getName());
//            }
//        });
//
//        for (Valute i : valCurs.getList()
//        ) {
//            System.out.println(i.getName());
//        }


    }

    public static void readFromExcel() throws Exception {
        FileInputStream inputStream = new FileInputStream(new File("D:\\stepanUniver\\cebanInternship\\ana\\Ana\\file.xls"));
        HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
        HSSFSheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            // Get iterator to all cells of current row
            Iterator<Cell> cellIterator = row.cellIterator();

            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                CellType cellType = cell.getCellType();
                switch (cellType) {
                    case _NONE:
                        System.out.print("");
                        System.out.print("\t");
                        break;
                    case BOOLEAN:
                        System.out.print(cell.getBooleanCellValue());
                        System.out.print("\t");
                        break;
                    case BLANK:
                        System.out.print("");
                        System.out.print("\t");
                        break;
                    case FORMULA:
                        // Formula
                        System.out.print(cell.getCellFormula());
                        System.out.print("\t");

                        FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
                        // Print out value evaluated by formula
                        System.out.print(evaluator.evaluate(cell).getNumberValue());
                        break;
                    case NUMERIC:
                        System.out.print(cell.getNumericCellValue());
                        System.out.print("\t");
                        break;
                    case STRING:
                        System.out.print(cell.getStringCellValue());
                        System.out.print("\t");
                        break;
                    case ERROR:
                        System.out.print("!");
                        System.out.print("\t");
                        break;
                }

            }
            System.out.println(" ");
        }
    }

} 