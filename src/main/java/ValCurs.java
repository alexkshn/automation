import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

public class ValCurs {
    @XStreamAsAttribute
    private String Date;
    @XStreamAsAttribute
    private String name;
    @XStreamImplicit
    private List<Valute> list;

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Valute> getList() {
        return list;
    }

    public void setList(List<Valute> list) {
        this.list = list;
    }

}
