import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Driver;
import java.util.*;

public class TestClass {
    private static final String FILE_PATH = "C:\\Users\\Elena\\Desktop\\testNGProj\\testNGProj\\Official_exchange.xls";
    private static final String XML_URL = "http://www.bnm.md/en/official_exchange_rates?get_xml=1&date=27.09.2017";
    private static final String EXCEL_URL = "http://www.bnm.md/en/export-official-exchange-rates?get_xml=1&date=27.09.2017&xls=1";
    ValCurs valCurs;

    @BeforeSuite
    public void beforeMethod() throws Exception {
        InputStream in = new URL(EXCEL_URL).openStream();
        Files.copy(in, Paths.get(FILE_PATH), StandardCopyOption.REPLACE_EXISTING);
        System.err.println("Hello before");

        URLConnection hp = new URL(XML_URL).openConnection();
        InputStream stream = hp.getInputStream();

        BufferedReader rd = new BufferedReader(new InputStreamReader(stream));
        StringBuilder resp = new StringBuilder(); // or StringBuffer if Java version 5+
        String line;
        while ((line = rd.readLine()) != null) {
            resp.append(line);
            resp.append('\n');
        }
        rd.close();

        XStream xstream = new XStream(new DomDriver());
        xstream.processAnnotations(new Class[]{ValCurs.class});
        valCurs = (ValCurs) xstream.fromXML(resp.toString());
    }

    @DataProvider(name = "excelReader")
    public Object[][] dataProviderMethod() throws Exception {
        return readFromExcel();
    }

    @Test(dataProvider = "excelReader")
    synchronized public void testMethod(String currency, String numCode, String charCode, String nominal, String value) throws Exception {
        Collections.sort(valCurs.getList(), new Comparator<Valute>() {
            public int compare(Valute v1, Valute v2) {
                return v1.getName().compareTo(v2.getName());
            }
        });

        for (Valute valute : valCurs.getList()) {
            if (valute.getName().equalsIgnoreCase(currency)){
                Assertions.assertThat(valute.getName()).isEqualToIgnoringCase(currency);
                Assertions.assertThat(valute.getCharCode()).isEqualToIgnoringCase(charCode);
                Assertions.assertThat(valute.getNominal()).isEqualToIgnoringCase(nominal);

                Double xmlValue = Double.parseDouble(valute.getValue());
                Double excelValue = Double.parseDouble(value);
                Assertions.assertThat(xmlValue).isEqualTo(excelValue);
                Assertions.assertThat(valute.getNumCode()).isEqualToIgnoringCase(numCode);
            }
        }
    }


        public Object[][] readFromExcel() throws Exception{
            System.err.println("Hello read");
            DataFormatter dataFormatter = new DataFormatter();
            FileInputStream inputStream = new FileInputStream(new File(FILE_PATH));
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            HSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();

            Object[][] dataExcel = new Object[100][100];
            int rowCount = 0;

            Map<String, List<String>> map = new HashMap<>();
            List<List<String>> listList = new ArrayList<>();


            while (rowIterator.hasNext()) {
                boolean flag = true;
                List<String> arrValues = new ArrayList<>();
                Row rowEx = rowIterator.next();
                Iterator<Cell> cellIterator = rowEx.cellIterator();

                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();

                        String cellValue = dataFormatter.formatCellValue(cell);
                        if (cellValue.contains("Currency") || cellValue.isEmpty() || cellValue.contains("exchange")
                    || cellValue.contains("Data source:")|| cellValue.contains("BNM")|| cellValue.contains("Date:")||cellValue.contains("Hour:")){
                            flag = false;
                            break;
                        }

                        cellValue = cellValue.replaceAll(",", "\\.");
                        arrValues.add(cellValue);
                    }

                    if(flag) {
                        listList.add(arrValues);
                        rowCount++;
                    }
            }


            String[][] array = new String[listList.size()][];
            for (int i = 0; i < array.length; i++) {
                array[i] = new String[listList.get(i).size()];
            }
            for(int i = 0; i < listList.size(); i++){
                for (int j = 0; j < listList.get(i).size(); j++) {
                    array[i][j] = listList.get(i).get(j);
                }
            }

            Object excelData [][] = (Object[][])array;
            return excelData;

        }
        @Test
        public void doChromeDriver() throws Exception {
            System.setProperty("webdriver.chrome.driver","/Users/acusnir/Downloads/testNGProj/chromedriver");


            WebDriver driver = new ChromeDriver();
            driver.get("https://www.bnm.md/en/content/official-exchange-rates");

        }
}
